import { all, takeEvery, takeLatest } from 'redux-saga/effects';
import { logoutSaga, checkAuthTimeoutSaga, authUserSaga, authCheckStateSaga } from './auth';
import { initIngredientsSaga } from './burgerBuilder';
import { fetchOrdersSaga, purchaseBurgerSaga } from './order';
import * as actionTypes from '../actions/actionTypes';

export function* authSaga() {
    yield all([
        yield takeEvery(actionTypes.AUTH_LOGOUT_START, logoutSaga),
        yield takeEvery(actionTypes.AUTH_TIMEOUT_CHECK, checkAuthTimeoutSaga),
        yield takeEvery(actionTypes.AUTH_INIT, authUserSaga),
        yield takeEvery(actionTypes.AUTH_CHECK_STATE, authCheckStateSaga)
    ]);
}

export function* burgerBuilderSaga() {
    yield takeEvery(actionTypes.INIT_INGREDIENTS, initIngredientsSaga);
}

export function* orderSaga() {
    yield takeEvery(actionTypes.FETCH_ORDERS_INIT, fetchOrdersSaga);
    yield takeLatest(actionTypes.PURCHASE_BURGER_PROCESS, purchaseBurgerSaga);
}