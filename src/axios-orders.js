import Axios from "axios";

const instance = Axios.create({
    baseURL: "https://react-learing-ol.firebaseio.com/"
})

// instance.interceptors.request.use(request => {
//     // console.log(request);
//     return request; //REQUIRED to don't block sending
// }, error => {
//     // console.log(error);
//     return Promise.reject(error);
// })

// instance.interceptors.response.use(response => {
//     // console.log(response);
//     return response; //REQUIRED
// }, error => {
//     // console.log(error);
//     return Promise.reject(error);
// })

export default instance;