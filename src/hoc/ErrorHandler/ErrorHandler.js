import React, { useState, useEffect } from 'react';
import Modal from '../../components/UI/Modal/Modal';
import Hoc from '../Hoc';

const ErrorHandler = (WrappedContent, axios) => {
    /* eslint-disable */
    return props => {
        const [error, setError] = useState(null);

        const requestInterceptor = axios.interceptors.request.use(req => {
            setError(null);
            return req;
        });
        const responseInterceptor = axios.interceptors.response.use(req => req, error => {
            setError(error);
        });

        useEffect(() => {
            return () => {
                axios.interceptors.request.eject(requestInterceptor);
                axios.interceptors.response.eject(responseInterceptor);
            }
        }, [requestInterceptor, responseInterceptor]);

        const errorConfirmedHandler = () => {
            setError(null);
        }

        return (
            <Hoc>
                <Modal
                    show={error}
                    closeModal={errorConfirmedHandler}>
                    {error ? error.message : null}
                </Modal>
                <WrappedContent {...props} />
            </Hoc>
        );
    };

}

export default ErrorHandler;
