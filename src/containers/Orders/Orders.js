import React, { useEffect } from 'react';
import Order from '../../components/Order/Order';
import { connect } from 'react-redux';

import * as actions from '../../store/actions/index'

import Axios from '../../axios-orders';
import ErrorHandler from '../../hoc/ErrorHandler/ErrorHandler';
import Spinner from '../../components/UI/Spinner/Spinner';

const Orders = props => {
    const { fetchOrders, token, userId } = props;
    useEffect(() => {
        fetchOrders(token, userId);
    }, [fetchOrders, token, userId]);

    let orders = <Spinner />;
    if (!props.loading) {
        orders = props.orders.map((elem) => (
            <Order
                key={elem.id}
                ingredients={elem.ingredients}
                price={elem.price} />
        ));
    }
    return (
        <div>
            {orders}
        </div>
    );
}

const mapStateToProps = state => {
    return {
        orders: state.order.orders,
        loading: state.order.loading,
        token: state.auth.token,
        userId: state.auth.userId
    }
}

const mapDispatchToProps = dispatch => {
    return {
        fetchOrders: (token, userId) => { dispatch(actions.fetchOrders(token, userId)) }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorHandler(Orders, Axios));