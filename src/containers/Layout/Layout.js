import React, { useState } from 'react';
import { connect } from 'react-redux';

import Hoc from '../../hoc/Hoc';
import classes from './Layout.css';
import Toolbar from '../../components/Navigation/Toolbar/Toolbar';
import SideDrawer from '../../components/Navigation/SideDrawer/SideDrawer';



const Layout = props => {
    const [isSideDrawerShown, setIsSideDrawerShown] = useState(false);

    const sideDrawerClosedHandler = () => {
        setIsSideDrawerShown(false);
    }

    const sideDrawerOpenHandler = () => {
        setIsSideDrawerShown(isSideDrawerShown => !isSideDrawerShown);
    }

    return (
        <Hoc>
            <SideDrawer
                isAuth={props.isAuthenticated}
                open={isSideDrawerShown}
                close={sideDrawerClosedHandler} />
            <Toolbar
                openSideDrawer={sideDrawerOpenHandler}
                isAuth={props.isAuthenticated} />
            <main className={classes.Content}>
                {props.children}
            </main>
        </Hoc>
    );
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token !== null
    }
}
export default connect(mapStateToProps)(Layout);