import React, { Fragment, useState, useEffect } from 'react';

import Hoc from '../../hoc/Hoc';
import Burger from '../../components/Burger/Burger';
import BuildControls from '../../components/Burger/BuildControls/BuildControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Axios from '../../axios-orders';
import Spinner from '../../components/UI/Spinner/Spinner';
import ErrorHandler from '../../hoc/ErrorHandler/ErrorHandler';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';


const BurgerBuilder = props => {
    const [inPurchase, setInPurchase] = useState(false);
    const { onInitIngredients } = props;
    useEffect(() => {
        onInitIngredients()
    }, [onInitIngredients]);

    const updatePurchasable = (ingredients) => {
        const sumOfIngredients = Object.keys(ingredients)
            .map(ingredient => {
                return ingredients[ingredient]
            }).reduce((sum, elem) => {
                return sum + elem;
            }, 0);
        return sumOfIngredients > 0;
    }

    const purchaseHandler = () => {
        if (props.isAuthenticated) {
            setInPurchase(true);
        } else {
            props.setAuthRedirectPath('/checkout');
            props.history.push('/signup')
        }
    };

    const purchaseCancelHandler = () => {
        setInPurchase(false);
    };

    const purchaseContinueHandler = () => {
        props.onPurchaseBurgerInit();
        props.history.push('/checkout');
    };

    const disabledInfo = {
        ...props.ings
    }
    for (let key in disabledInfo) {
        disabledInfo[key] = disabledInfo[key] <= 0;
    }
    let orderSummary = null;

    let burger = props.error ? <p style={{ textAlign: 'center' }}>The ingredients can't be loaded</p> : <Spinner />;
    if (props.ings) {
        burger = (
            <Fragment>
                <Burger ingredients={props.ings} />
                <BuildControls
                    isAuth={props.isAuthenticated}
                    addIngredient={props.onIngredientAdd}
                    removeIngredient={props.onIngredientRemove}
                    disabledInfo={disabledInfo}
                    price={props.price}
                    purchasable={updatePurchasable(props.ings)}
                    order={purchaseHandler} />
            </Fragment>
        );
        orderSummary = <OrderSummary
            ingredients={props.ings}
            purchaseCancelHandler={purchaseCancelHandler}
            purchaseContinueHandler={purchaseContinueHandler}
            price={props.price} />;
    }

    return (
        <Hoc>
            <Modal show={inPurchase} closeModal={purchaseCancelHandler}>
                {orderSummary}
            </Modal>
            {burger}
        </Hoc>
    );

}

const mapStateToProps = state => {
    return {
        ings: state.burgerBuilder.ingredients,
        price: state.burgerBuilder.totalPrice,
        error: state.burgerBuilder.error,
        isAuthenticated: state.auth.token !== null
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onIngredientAdd: (ingredientName) => dispatch(actions.addIngredient(ingredientName)),
        onIngredientRemove: (ingredientName) => dispatch(actions.removeIngredient(ingredientName)),
        onInitIngredients: () => dispatch(actions.initIngredients()),
        onPurchaseBurgerInit: () => dispatch(actions.purchaseBurgerInit()),
        setAuthRedirectPath: (path) => dispatch(actions.setAuthRedirectPath(path))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ErrorHandler(BurgerBuilder, Axios));