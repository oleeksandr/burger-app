import React, { useEffect, Suspense } from 'react';
import { Route, Switch, Redirect, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';

import * as actions from './store/actions/index';

import Layout from './containers/Layout/Layout';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Logout from './containers/Auth/Logout';
import Auth from './containers/Auth/Auth';


const AsyncOrders = React.lazy(() => {
  return import('./containers/Orders/Orders');
});
const AsyncCheckout = React.lazy(() => {
  return import('./containers/Checkout/Checkout');
});

const App = props => {
  const { authCheckState } = props;
  useEffect(() => {
    authCheckState();
  }, [authCheckState]);

  let routes = (
    <Switch>
      <Route key="/signup" path="/signup" render={props => <Auth {...props} isSignUp={true} />} />
      <Route key="/signin" path="/signin" render={props => <Auth {...props} isSignUp={false} />} />
      <Route key="/" path="/" exact component={BurgerBuilder} />
      {/* <Redirect key="/redirect" to="/" /> */}
      <Route key="/redirect" path="/" component={BurgerBuilder} />
    </Switch>);

  if (props.isAuthenticated) {
    routes = (
      <Switch>
        <Route key="/signup" path="/signup" render={props => <Auth {...props} isSignUp={true} />} />
        <Route key="/signin" path="/signin" render={props => <Auth {...props} isSignUp={false} />} />
        <Route key="/logout" path="/logout" render={props => <Logout {...props} />} />
        <Route key="/checkout" path="/checkout" render={props => <AsyncCheckout {...props} />} />
        <Route key="/orders" path="/orders" render={props => <AsyncOrders {...props} />} />
        <Route key="/" path="/" exact component={BurgerBuilder} />
        <Redirect key="/redirect" to="/" />
        {/* <Route key="/redirect" path="/" component={BurgerBuilder} /> */}
      </Switch>);
  }

  return (
    <div>
      <Layout>
        <Suspense fallback={<p> Loading... </p>}> {routes} </Suspense>
      </Layout>
    </div>
  );

}

const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token !== null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    authCheckState: () => dispatch(actions.authCheckState())
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
