import React from 'react';

import classes from "./Burger.css";
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';

const Burger = (props) => {

    let ingredientsList = Object.keys(props.ingredients)
        .map(ingredient =>
            [...Array(props.ingredients[ingredient])].map((el, i) =>
                <BurgerIngredient key={ingredient + i} type={ingredient} />
            )
        ).reduce((arr, el) => arr.concat(el), []);

    if (ingredientsList.length === 0) {
        ingredientsList = <p>Please add some ingredients</p>;
    }
    return (
        <div className={classes.Burger}>
            <BurgerIngredient type="bread-top" />
            {ingredientsList}
            <BurgerIngredient type="bread-bottom" />
        </div>
    )
}

export default Burger;