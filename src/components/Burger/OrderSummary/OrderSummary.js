import React from 'react';

import Button from '../../UI/Button/Button';

const OrderSummary = props => {

    const ingredientSummary = Object.keys(props.ingredients)
        .map(elem => {
            return (
                <li key={elem}>
                    <span style={{ textTransform: 'capitalize' }}>{elem}</span>: {props.ingredients[elem]}
                </li>
            );
        });

    return (
        <div>
            <React.Fragment>
                <h3>Your Order</h3>
                <p>contains the following ingredients:</p>
                <ul>
                    {ingredientSummary}
                </ul>
                <p><strong>Total Price: {props.price.toFixed(2)}</strong></p>
                <p>Continue for checkout?</p>
                <Button btnType="Danger" clicked={props.purchaseCancelHandler}>Cancel</Button>
                <Button btnType="Success" clicked={props.purchaseContinueHandler}>Continue</Button>
            </React.Fragment>
        </div >
    );
};

export default OrderSummary;