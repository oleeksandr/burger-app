import React from 'react';

import classes from './BuildControls.css';
import BuildControl from './BuildControl/BuildControl';

const controls = [
    { label: 'Salad', type: 'salad' },
    { label: 'Bacon', type: 'bacon' },
    { label: 'Cheese', type: 'cheese' },
    { label: 'Meat', type: 'meat' }
]

const BuildControls = (props) => {
    return (
        <div className={classes.BuildControls}>
            <p>Current Price: <strong>{props.price.toFixed(2)}</strong></p>
            {controls.map(elem => (
                <BuildControl
                    key={elem.label}
                    label={elem.label}
                    add={() => props.addIngredient(elem.type)}
                    remove={() => props.removeIngredient(elem.type)}
                    disabled={props.disabledInfo[elem.type]} />
            ))}
            <button className={classes.OrderButton}
                disabled={!props.purchasable}
                onClick={props.order}>{props.isAuth? "ORDER NOW" : "SIGN UP TO ORDER"}</button>

        </div>
    );
};

export default BuildControls;