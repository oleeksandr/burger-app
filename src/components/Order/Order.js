import React from 'react';
import classes from './Order.css';

const Order = (props) => {

    // { bacon, cheese, meat, salad }
    const ingredients = [];
    for (let ingredientName in props.ingredients) {
        ingredients.push({
            name: ingredientName,
            amount: props.ingredients[ingredientName]
        });
    }
    const orderOutput = ingredients.map((ingredient) => `${ingredient.name}: ${ingredient.amount}`).join(', ');

    return (
        <div className={classes.Order}>
            <p>Ingredients: {orderOutput}</p>

            <p>Price: <strong>{Number.parseFloat(props.price).toFixed(2)}$</strong></p>
        </div>
    );
}

export default Order;
