import React from 'react'
import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button';

const CheckoutSummary = (props) => {
    return (
        <div style={{ textAlign: "center" }}>
            <h1>We hope it tastes well!</h1>
            <div>
                <Burger ingredients={props.ingredients} />
            </div>
            <Button
                btnType="Danger"
                clicked={props.checkoutCanceled}>Cancel</Button>
            <Button
                btnType="Success"
                clicked={props.checkoutContinued}>Order</Button>
        </div>
    )
}

export default CheckoutSummary;
