import React from 'react';

import classes from './SideDrawer.css';
import Logo from '../../Logo/Logo';
import NavigationItems from '../NavigationItems/NavigationItems';
import Backdrop from '../../UI/Backdrop/Backdrop';

const SideDrawer = (props) => {
    let attachedClasses = [classes.SideDrawer, classes.Close];
    if (props.open) {
        attachedClasses = [classes.SideDrawer, classes.Open];
    }

    return (
        <React.Fragment>
            <Backdrop show={props.open} close={props.close} />
            <div className={attachedClasses.join(' ')}>
                {/* <div className={classes.SideDrawer, !props.open? classes.Open : classes.Close].join('')}> */}
                <div className={classes.SideDrawer__Logo}>
                    <Logo />
                </div>
                <nav onClick={props.close}>
                    <NavigationItems isAuth={props.isAuth}/>
                </nav>
            </div>
        </React.Fragment>
    );
};

export default SideDrawer;