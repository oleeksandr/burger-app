import React from 'react';

import classes from './NavigationItems.css';
import NavigationItem from './NavigationItem/NavigationItem';
import Hoc from '../../../hoc/Hoc';

const NavigationItems = (props) => {

    return (
        <ul className={classes.NavigationItems}>
            <NavigationItem link="/" exact> Burger Builder </NavigationItem>
            {!props.isAuth ? (
                <Hoc>
                    <NavigationItem link="/signup"> Sign Up </NavigationItem>
                    <NavigationItem link="/signin"> Sign In </NavigationItem>
                </Hoc>
            ) : (
                    <Hoc>
                        <NavigationItem link="/orders"> Orders </NavigationItem>
                        <NavigationItem link="/logout"> Sign Out </NavigationItem>
                    </Hoc>
                )}
        </ul>
    );
};

export default NavigationItems;