import React from 'react';

import classes from './Modal.css';
import Hoc from '../../../hoc/Hoc';
import Backdrop from '../Backdrop/Backdrop';

const Modal = props => (
    <Hoc>
        <Backdrop show={props.show} close={props.closeModal} />
        <div className={classes.Modal}
            style={{
                opacity: props.show ? '1' : '0',
                transform: props.show ? "translateY(0)" : "translateY(-100vh)"
            }}>
            {props.children}
        </div>
    </Hoc>
);

export default React.memo(Modal, (prevProps, nextProps) =>
    nextProps.show === prevProps.show && nextProps.children === prevProps.children
);